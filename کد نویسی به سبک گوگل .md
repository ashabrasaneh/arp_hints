# کد نویسی به سبک گوگل

در این صفحه اصول کد نویسی به سبک و استایل گوگل گفته میشه. تمامی کد ها در شرکت اصحاب رسانه پویا باید در قالبی که در این صفحه تبیین میشه نوشته بشن.

### مقدمه

خب حالا چرا باید اصلا از قالب یا استایل خاصی برای کد هایی که مینویسیم استفاده کنیم ؟ واقعیتش اینه که شما تا زمانی که کدتون ارور نداشته باشه میتونین به هر شکلی که فکر میکنین کد بنویسین میتونین به جای یک Tab در ابتدای هر بلاک دوتا بزارین یا هر چنتا که فکرش رو میکنین و اصلا هم برنامه ارور نمیده و مشکل خاصی براش پیش نمیاد! پس چرا؟!

**به این دلیل که کد شما فقط توسط شما خونده نمیشه!**

کد شما رو ممکنه چندین نفر دیگه هم مطالعه کنن؛ هم ممکنه برای یادگیری و هم ممکنه برای دیباگ و یا حتی توسعه بیشتر در آینده. حالا فرض کنید هر کس دست خط خودش رو داشته باشه و به زبان خودش بنویسه : این کار باعث میشه نفر بعدی نتونه به سرعت با کد ارتباط برقرار کنه و اون رو درک کنه و ممکنه نیاز باشه بار ها اون کد رو بالا و پایین کنه تا متوجه بشه کدوم کد مال کجاست!

این کار به شدت روی سرعت عملیات های آینده روی کد شما رو کند میکنه و حتی ممکنه نفر بعدی اصلا نتونه روی د شما کار کنه و مجبور بشه دوباره اون کد رو از اول بزنه! این یعنی ضرر شرکت ، یعنی ضرر یک تیم ، تیمی که خودمون هم توش کار میکنیم، تیمه که پیشرفتش یعنی پیشرفت خودمون و ضررش قطعا ضرر خودمون هم هست. 

سوال دوم چرا حالا استایل گوگل؟ چرا از استایل مایکروسافت استفاده نکنیم؟ جوابش اینه که واقعیتش استایل گوگل نوع کمی تغییر یافته استایل مایکروسافت هست. با توجه به یک سری تغییراتی که روی استایل مایکروسافت داده شده کد به مراتب حجم کمتری داره و راحت تر خونده میشه.

**تعاریف اولیه** 

کمل کیس و پاسکال کیس : camelCase یعنی حرف اول کلمه با حرف کوچک انگلیسی نوشته میشه و بعد از اتمام کلمه اول بدون فاصله کلمه دوم با حرف بزرگ شروع میشه ولی در PascalCase حرف اول هم بزرگ هستش.

**نکته :** تحت هیچ شرایطی نام چیزی رو فینگلیش ننویسید!! همیشه توی کد باید اسامی به زبان انگلیسی نوشته بشن.

هشدار : هیچ شرایطی اونقدر استثنایی نیست که براش از نظم و ترتیب کدتون کم کنید؛ پس هیچ وقت به هیچ بهونه ای این قوانین رو زیر پا نزارین.

### نام گذاری

- نام متغیر ها و پارامتر های local باید به صورت camelCase نوشته شود.
- نام متغیر های private , internal و protected internal باید به صورت _camelCase نوشته شود (به underscore قبل از نام دقت کنید).
- نام گذاری متغیر ها تحت تاثیر مدیفایر های const , static , readonly و غیره قرار نمیگیرد.
- نام کلاس ها ، متد ها ، enum ها ، متغیر های public و namespace ها باید به صورت PascalCase نوشته شود.
- هیچگاه دو حرف بزرگ در کنار یکدیگر نوشته نمیشوند.
- در بین کلمات اسامی هیچگاه از " _ " استفاده نمیشود.
- نام Interface ها همیشه به صورت PascalCase و با یک حرف پیشوند " I " نوشته میشود.
- نام فایل ها همیشه PascalCase نوشته میشود.
- همیشه در هر فایل یک کلاس اصلی قرار میگیرد که دقیقا باید هم نام فایل باشد.
- در کل سعی کنید همیشه در هر فایل فقط یک کلاس نوشته شود.

### ترتیب قرار گیری

- متد ها و متغیر ها و ... با ترتیب زیر در کد قرار داده میشوند:

public → protected → Internal  → Internal → New → Abstract → Virtual → Override → Sealed → Static → Readonly → Extern → Unsafe → Volatile → Async

- اول از هر چیز در کد NameSpace هایی که با using در کد استفاده میشوند نوشته میشود.
- ترتیب وارد کردن NameSpace ها به ترتیب الفبا است به استثنا System ها که اول از همه نوشته میشوند.
- اعضای کلاس ها به صورت زیر ترتیب بندی میشوند :
    - کلاس ها ، enum  ها ، delegate ها و event های داخلی.
    - متغیر های static ,const و readonly.
    - متغیر ها و فیلد ها.
    - Constructor ها و finalizer ها.
    - متد ها.
- در هر کدام از گروه ها اعضا باید به ترتیب زیر قرار گرفته باشندک
    - public
    - intermal
    - protected internal
    - protected
    - private

### فضا های خالی و Space ها

- در هر خط فقط یک عبارت کامل قرار بگیرد.
- در هر خط فقط یک مقدار دهی و تعریف قرار بگیرد.
- هر Indentation (تو رفتگی) شامل 2 Space و در کد به هیچ عنوان از کاراکتر Tab استفاده نمیشود.
- حداکثر طول خط **در همه شرایط بدون استثنا** فقط 100 حرف میباشد.
- قبل از باز کردن " } " خط جدید ایجاد نمیشود.
- بعد از بستن " { " و بین else خط اضافه نمیشود و در ادامه بلاک نوشته میشود.
- "{}" حتی زمانی که اختیاری هستند هم میتوانند استفاده شوند.
- بعد از if , for , while و غیره (قبل از باز شدن پرانتز) space قرار میگیرد و همچنین بعد از " , " کاما.
- بعد از باز شدن پرانتز یا قبل از بسته شدن پرانتز space قرار نمیگیرد.
- بین عملگر های Unery (+, -,!, ~, ++, — یعنی عملگر هایی که فقط یک عملوند دارند) و عملوند هیچ فاصله ای قرار نمیگیرد و بین بقیه عملگر ها و عملوند ها یک فاصله قرار میگیرد.
- خطوطی که در خط بعد امتداد دارند باید با چهار فاصله در خط بعد شروع شوند.

```csharp
using System;                                       // `using` goes at the top, outside the
                                                    // namespace.

namespace MyNamespace {                             // Namespaces are PascalCase.
                                                    // Indent after namespace.
  public interface IMyInterface {                   // Interfaces start with 'I'
    public int Calculate(float value, float exp);   // Methods are PascalCase
                                                    // ...and space after comma.
  }

  public enum MyEnum {                              // Enumerations are PascalCase.
    Yes,                                            // Enumerators are PascalCase.
    No,
  }

  public class MyClass {                            // Classes are PascalCase.
    public int Foo = 0;                             // Public member variables are
                                                    // PascalCase.
    public bool NoCounting = false;                 // Field initializers are encouraged.
    private class Results {
      public int NumNegativeResults = 0;
      public int NumPositiveResults = 0;
    }
    private Results _results;                       // Private member variables are
                                                    // _camelCase.
    public static int NumTimesCalled = 0;
    private const int _bar = 100;                   // const does not affect naming
                                                    // convention.
    private int[] _someTable = {                    // Container initializers use a 2
      2, 3, 4,                                      // space indent.
    }

    public MyClass() {
      _results = new Results {
        NumNegativeResults = 1,                     // Object initializers use a 2 space
        NumPositiveResults = 1,                     // indent.
      };
    }

    public int CalculateValue(int mulNumber) {      // No line break before opening brace.
      var resultValue = Foo * mulNumber;            // Local variables are camelCase.
      NumTimesCalled++;
      Foo += _bar;

      if (!NoCounting) {                            // No space after unary operator and
                                                    // space after 'if'.
        if (resultValue < 0) {                      // Braces used even when optional and
                                                    // spaces around comparison operator.
          _results.NumNegativeResults++;
        } else if (resultValue > 0) {               // No newline between brace and else.
          _results.NumPositiveResults++;
        }
      }

      return resultValue;
    }

    public void ExpressionBodies() {
      // For simple lambdas, fit on one line if possible, no brackets or braces required.
      Func<int, int> increment = x => x + 1;

      // Closing brace aligns with first character on line that includes the opening brace.
      Func<int, int, long> difference1 = (x, y) => {
        long diff = (long)x - y;
        return diff >= 0 ? diff : -diff;
      };

      // If defining after a continuation line break, indent the whole body.
      Func<int, int, long> difference2 =
          (x, y) => {
            long diff = (long)x - y;
            return diff >= 0 ? diff : -diff;
          };

      // Inline lambda arguments also follow these rules. Prefer a leading newline before
      // groups of arguments if they include lambdas.
      CallWithDelegate(
          (x, y) => {
            long diff = (long)x - y;
            return diff >= 0 ? diff : -diff;
          });
    }

    void DoNothing() {}                             // Empty blocks may be concise.

    // If possible, wrap arguments by aligning newlines with the first argument.
    void AVeryLongFunctionNameThatCausesLineWrappingProblems(int longArgumentName,
                                                             int p1, int p2) {}

    // If aligning argument lines with the first argument doesn't fit, or is difficult to
    // read, wrap all arguments on new lines with a 4 space indent.
    void AnotherLongFunctionNameThatCausesLineWrappingProblems(
        int longArgumentName, int longArgumentName2, int longArgumentName3) {}

    void CallingLongFunctionName() {
      int veryLongArgumentName = 1234;
      int shortArg = 1;
      // If possible, wrap arguments by aligning newlines with the first argument.
      AnotherLongFunctionNameThatCausesLineWrappingProblems(shortArg, shortArg,
                                                            veryLongArgumentName);
      // If aligning argument lines with the first argument doesn't fit, or is difficult to
      // read, wrap all arguments on new lines with a 4 space indent.
      AnotherLongFunctionNameThatCausesLineWrappingProblems(
          veryLongArgumentName, veryLongArgumentName, veryLongArgumentName);
    }
  }
}
```